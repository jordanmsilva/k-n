package com.gmail.jordansilva.config;

import static java.lang.String.format;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.reactive.function.client.WebClient;

import com.gmail.jordansilva.web.client.RestClient;

@Configuration
public class TestBeanConfig {

	@Bean
	@Primary
	public RestClient coinDeskClientMock(
			WebClient.Builder webClientBuilder,
			@Value("${wiremock.server.port}") int port) {
		return new RestClient(webClientBuilder, format("http://localhost:%s/", port));
	}
}
