package com.gmail.jordansilva;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.gmail.jordansilva.utils.CoinDeskResponses.CURRENT_RATE_BRL_JSON;
import static com.gmail.jordansilva.utils.CoinDeskResponses.CURRENT_RATE_USD_JSON;
import static com.gmail.jordansilva.utils.CoinDeskResponses.HISTORICAL_RATE_JSON;
import static com.gmail.jordansilva.utils.CoinDeskResponses.SUPPORTED_CURRENCIES_JSON;
import static com.gmail.jordansilva.utils.Currencies.BRL;
import static com.gmail.jordansilva.utils.Currencies.USD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;

import javax.inject.Inject;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;

import com.gmail.jordansilva.Application.UnsupportedCurrencyException;
import com.gmail.jordansilva.domain.model.Currency;
import com.gmail.jordansilva.domain.model.Period;

@SpringBootTest
@ActiveProfiles("IntegrationTest")
@AutoConfigureWireMock(port = 0)
public class ApplicationTest {

	private static final Currency UNSUPPORTED_CURRENCY = new Currency("UUU");

	@Inject
	Application app;

	@BeforeEach
	void setup() {
		stubSupportedCurrencies();
	}

	@Test
	public void getRateShouldReturnCurrentPriceInUSDWhenPassinUSDAsCurrency() {
		stubGetRateForUSD();
		assertGetRate(USD, new Double(10404.7433));
	}

	@Test
	public void getRateShouldReturnCurrentPriceInBRLWhenPassingBRLAsCurrency() {
		stubGetRateForBRL();
		assertGetRate(BRL, new Double(42239.3539));
	}

	@Test
	public void getRateShouldThrowErrorIfCurrencyIsNotSupported() {
		assertThrowsUnsupportedCurrencyException(() -> {
			app.getRate(UNSUPPORTED_CURRENCY);
		});
	}

	@Test
	public void getHistoricalLowShouldReturnTheLowestRateInThePeriod() {
		stubGetHistoricalRate();
		Double lowestRate = app.getHistoricalLow(BRL, getPeriod());
		assertThat(lowestRate).isEqualTo(39608.1554);
	}

	@Test
	public void getHistoricalLowShouldThrowErrorIfCurrencyIsNotSupported() {
		assertThrowsUnsupportedCurrencyException(() -> {
			app.getHistoricalLow(UNSUPPORTED_CURRENCY, getPeriod());
		});
	}

	@Test
	public void getHistoricalHighShouldReturnTheHighestRateInThePeriod() {
		stubGetHistoricalRate();
		Double highestRate = app.getHistoricalHigh(BRL, getPeriod());
		assertThat(highestRate).isEqualTo(47558.1992);
	}

	@Test
	public void getHistoricalHighShouldThrowErrorIfCurrencyIsNotSupported() {
		assertThrowsUnsupportedCurrencyException(() -> {
			app.getHistoricalHigh(UNSUPPORTED_CURRENCY, getPeriod());
		});
	}

	private void assertThrowsUnsupportedCurrencyException(Executable executable) {
		assertThrows(UnsupportedCurrencyException.class, executable);
	}

	private void assertGetRate(Currency currency, Double expectedRate) {
		Double rate = app.getRate(currency);
		assertThat(rate).isEqualTo(expectedRate);
	}

	private void stubGetRateForUSD() {
		stubGetRate(USD, CURRENT_RATE_USD_JSON);
	}

	private void stubGetRateForBRL() {
		stubGetRate(BRL, CURRENT_RATE_BRL_JSON);
	}

	private void stubGetRate(Currency currency, String response) {
		stub(String.format("/currentprice/%s.json", currency.getCode()), "application/javascript", response);
	}

	private void stubSupportedCurrencies() {
		stub("/supported-currencies.json", "text/html; charset=UTF-8", SUPPORTED_CURRENCIES_JSON);
	}

	private void stubGetHistoricalRate() {
		stub("/historical/close.json?currency=BRL&start=2019-08-01&end=2019-09-01",
				"application/javascript",
				HISTORICAL_RATE_JSON);
	}

	private void stub(String path, String contentType, String response) {
		stubFor(get(path).willReturn(aResponse().withHeader("Content-Type", contentType).withBody(response)));
	}

	private Period getPeriod() {
		LocalDate start = LocalDate.of(2019, 8, 1);
		LocalDate end = LocalDate.of(2019, 9, 1);
		Period period = new Period(start, end);
		return period;
	}
}
