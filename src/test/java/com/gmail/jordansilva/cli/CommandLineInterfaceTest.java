package com.gmail.jordansilva.cli;

import static com.gmail.jordansilva.utils.Currencies.BRL;
import static com.gmail.jordansilva.utils.Currencies.USD;
import static java.time.LocalDate.now;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.gmail.jordansilva.Application;
import com.gmail.jordansilva.domain.model.Period;

@ExtendWith(MockitoExtension.class)
public class CommandLineInterfaceTest {

	@Mock
	private Interactor interactor;

	@Mock
	private Application app;

	private CommandLineInterface cli;

	@BeforeEach
	public void setup() {
		cli = new CommandLineInterface(app, interactor, new EnglishMessenger());
	}

	@Test
	public void cliShouldAskToEnterCurrencyCode() throws Exception {
		mockInteractions();
		cli.run((String[]) null);
		verify(interactor).write("Enter the currency code (USD, EUR, BRL, etc): ");
	}

	@Test
	public void cliShouldPrintTheCurrentRateForEnteredCurrencyCodeWith4DigitsPrecision() throws Exception {
		mockInteractions();
		cli.run((String[]) null);
		verify(interactor).write("Current rate: 123,4560\n");
	}

	@Test
	public void cliShouldPrintTheHighestRateInTheLast30DaysForEnteredCurrencyCode() throws Exception {
		mockInteractions();
		cli.run((String[]) null);
		verify(interactor).write("Highest rate in the last 30 days: 99.123,4563\n");
	}

	@Test
	public void cliShouldPrintTheLowestRateInTheLast30DaysForEnteredCurrencyCode() throws Exception {
		mockInteractions();
		cli.run((String[]) null);
		verify(interactor).write("Lowest rate in the last 30 days: 123,2234\n");
	}

	@Test
	public void cliShouldPrintMessageWhenEnteredCurrencyCodeIsNotSupported() throws Exception {
		when(interactor.read()).thenReturn(USD.getCode());
		when(app.getRate(USD)).thenThrow(Application.UnsupportedCurrencyException.class);

		cli.run((String[]) null);
		verify(interactor).write("Currency \"USD\" is not supported.\n");
	}

	private void mockInteractions() {
		LocalDate today = now();
		Period last30daysPeriod = new Period(today.minusDays(30), today);

		when(interactor.read()).thenReturn(BRL.getCode());
		when(app.getRate(BRL)).thenReturn(123.456);
		when(app.getHistoricalHigh(BRL, last30daysPeriod)).thenReturn(99123.4563);
		when(app.getHistoricalLow(BRL, last30daysPeriod)).thenReturn(123.22344);
	}
}
