package com.gmail.jordansilva.domain.model.coinDesk;

import static java.time.LocalDate.parse;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import com.gmail.jordansilva.domain.model.coinDesk.HistoricalPriceResponse;

@JsonTest
public class HistoricalPriceResponseTest {

	@Inject
	private JacksonTester<HistoricalPriceResponse> json;

	private final String jsonResponse = "{  " +
			"   \"bpi\":{  " +
			"      \"2019-08-01\":39960.1558," +
			"      \"2019-08-02\":40921.719," +
			"      \"2019-08-03\":42058.7704," +
			"      \"2019-08-04\":42690.8628," +
			"      \"2019-08-05\":46920.0976" +
			"   }," +
			"   \"disclaimer\":\"This data was produced from the CoinDesk. BPI value data as BRL.\"," +
			"   \"time\":{  " +
			"      \"updated\":\"Sep 2, 2019 00:03:00 UTC\"," +
			"      \"updatedISO\":\"2019-09-02T00:03:00+00:00\"" +
			"   }" +
			"}";

	private HistoricalPriceResponse objectResponse = buildObjectResponse();

	@Test
	public void deserialize() throws IOException {
		assertThat(json.parse(jsonResponse)).isEqualTo(objectResponse);
	}

	private HistoricalPriceResponse buildObjectResponse() {
		return new HistoricalPriceResponse(getResponseValues());
	}

	private Map<LocalDate, Double> getResponseValues() {
		Map<LocalDate, Double> values = new HashMap<>();
		values.put(parse("2019-08-01"), 39960.1558);
		values.put(parse("2019-08-02"), 40921.719);
		values.put(parse("2019-08-03"), 42058.7704);
		values.put(parse("2019-08-04"), 42690.8628);
		values.put(parse("2019-08-05"), 46920.0976);
		return values;
	}
}
