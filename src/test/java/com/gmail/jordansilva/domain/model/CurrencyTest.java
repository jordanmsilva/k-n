package com.gmail.jordansilva.domain.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import com.gmail.jordansilva.domain.model.Currency;

@JsonTest
public class CurrencyTest {

	@Inject
	private JacksonTester<Currency> json;

	@Test
	public void deserialize() throws IOException {
		String content = "{\"currency\":\"AED\",\"country\":\"United Arab Emirates Dirham\"}";
		assertThat(json.parse(content)).isEqualTo(new Currency("AED"));
	}
}
