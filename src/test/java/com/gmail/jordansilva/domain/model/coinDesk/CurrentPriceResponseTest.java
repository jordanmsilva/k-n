package com.gmail.jordansilva.domain.model.coinDesk;

import static com.gmail.jordansilva.utils.CoinDeskResponses.CURRENT_RATE_BRL;
import static com.gmail.jordansilva.utils.CoinDeskResponses.CURRENT_RATE_BRL_JSON;
import static com.gmail.jordansilva.utils.CoinDeskResponses.CURRENT_RATE_USD;
import static com.gmail.jordansilva.utils.CoinDeskResponses.CURRENT_RATE_USD_JSON;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;

import com.gmail.jordansilva.domain.model.coinDesk.CurrentPriceResponse;

@JsonTest
public class CurrentPriceResponseTest {

	@Inject
	private JacksonTester<CurrentPriceResponse> json;

	@Test
	public void deserializeBRL() throws IOException {
		assertThat(json.parse(CURRENT_RATE_BRL_JSON)).isEqualTo(CURRENT_RATE_BRL);
	}

	@Test
	public void deserializeUSD() throws IOException {
		assertThat(json.parse(CURRENT_RATE_USD_JSON)).isEqualTo(CURRENT_RATE_USD);
	}
}
