package com.gmail.jordansilva.utils;

import com.gmail.jordansilva.domain.model.Currency;

public class Currencies {

	public static final Currency USD = new Currency("USD");
	public static final Currency BRL = new Currency("BRL");
}
