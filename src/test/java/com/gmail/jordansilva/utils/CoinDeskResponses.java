package com.gmail.jordansilva.utils;

import com.gmail.jordansilva.domain.model.coinDesk.CurrentPriceBPI;
import com.gmail.jordansilva.domain.model.coinDesk.CurrentPriceResponse;

public class CoinDeskResponses {

	public static final String CURRENT_RATE_USD_JSON = "{  " +
			"   \"time\":{  }," +
			"   \"disclaimer\":\"This data was produced ...\"," +
			"   \"bpi\":{  " +
			"      \"USD\":{  " +
			"         \"code\":\"USD\"," +
			"         \"rate\":\"10,404.7433\"," +
			"         \"description\":\"United States Dollar\"," +
			"         \"rate_float\":10404.7433" +
			"      }" +
			"   }" +
			"}";

	public static final CurrentPriceResponse CURRENT_RATE_USD = new CurrentPriceResponse(
			new CurrentPriceBPI(10404.7433));

	public static final String CURRENT_RATE_BRL_JSON = "{  " +
			"   \"time\":{  " +
			"      \"updated\":\"Sep 7, 2019 13:30:00 UTC\"," +
			"      \"updatedISO\":\"2019-09-07T13:30:00+00:00\"," +
			"      \"updateduk\":\"Sep 7, 2019 at 14:30 BST\"" +
			"   }," +
			"   \"disclaimer\":\"This data was produced...\"," +
			"   \"bpi\":{  " +
			"      \"USD\":{  " +
			"         \"code\":\"USD\"," +
			"         \"rate\":\"10,398.9150\"," +
			"         \"description\":\"United States Dollar\"," +
			"         \"rate_float\":10398.915" +
			"      }," +
			"      \"BRL\":{  " +
			"         \"code\":\"BRL\"," +
			"         \"rate\":\"42,239.3539\"," +
			"         \"description\":\"Brazilian Real\"," +
			"         \"rate_float\":42239.3539" +
			"      }" +
			"   }" +
			"}";

	public static final CurrentPriceResponse CURRENT_RATE_BRL = new CurrentPriceResponse(
			new CurrentPriceBPI(42239.3539));

	public static final String SUPPORTED_CURRENCIES_JSON = "[  " +
			"   {  " +
			"      \"currency\":\"AED\"," +
			"      \"country\":\"United Arab Emirates Dirham\"" +
			"   }," +
			"   {  " +
			"      \"currency\":\"AFN\"," +
			"      \"country\":\"Afghan Afghani\"" +
			"   }," +
			"   {  " +
			"      \"currency\":\"USD\"," +
			"      \"country\":\"Albanian Lek\"" +
			"   }," +
			"   {  " +
			"      \"currency\":\"AMD\"," +
			"      \"country\":\"Armenian Dram\"" +
			"   }," +
			"   {  " +
			"      \"currency\":\"BRL\"," +
			"      \"country\":\"Netherlands Antillean Guilder\"" +
			"   }," +
			"   {  " +
			"      \"currency\":\"AOA\"," +
			"      \"country\":\"Angolan Kwanza\"" +
			"   }" +
			"]";

	public static final String HISTORICAL_RATE_JSON = "{  " +
			"   \"bpi\":{  " +
			"      \"2019-08-01\":40944.7609," +
			"      \"2019-08-02\":40921.719," +
			"      \"2019-08-03\":42058.7704," +
			"      \"2019-08-04\":42690.8628," +
			"      \"2019-08-05\":46920.0976," +
			"      \"2019-08-06\":45425.0115," +
			"      \"2019-08-07\":47558.1992," +
			"      \"2019-08-08\":47077.917," +
			"      \"2019-08-09\":46786.6243," +
			"      \"2019-08-10\":44503.1685," +
			"      \"2019-08-11\":45584.3529," +
			"      \"2019-08-12\":45420.8891," +
			"      \"2019-08-13\":43108.7942," +
			"      \"2019-08-14\":40660.4653," +
			"      \"2019-08-15\":41145.0875," +
			"      \"2019-08-16\":41498.2638," +
			"      \"2019-08-17\":39960.1558," +
			"      \"2019-08-18\":41358.795," +
			"      \"2019-08-19\":44513.2236," +
			"      \"2019-08-20\":43688.6034," +
			"      \"2019-08-21\":40800.0644," +
			"      \"2019-08-22\":41165.7796," +
			"      \"2019-08-23\":42879.9451," +
			"      \"2019-08-24\":41820.9631," +
			"      \"2019-08-25\":41772.3658," +
			"      \"2019-08-26\":43087.5279," +
			"      \"2019-08-27\":41996.1632," +
			"      \"2019-08-28\":40533.194," +
			"      \"2019-08-29\":39608.1554," +
			"      \"2019-08-30\":39746.3924," +
			"      \"2019-08-31\":39831.684," +
			"      \"2019-09-01\":40518.1529" +
			"   }," +
			"   \"disclaimer\":\"This data was produced...\"," +
			"   \"time\":{  " +
			"      \"updated\":\"Sep 2, 2019 00:03:00 UTC\"," +
			"      \"updatedISO\":\"2019-09-02T00:03:00+00:00\"" +
			"   }" +
			"}";
}
