package com.gmail.jordansilva.web.client;

import java.util.List;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;

public class RestClient {

	private final WebClient webClient;

	public RestClient(WebClient.Builder webClientBuilder, String baseUrl) {
		this.webClient = webClientBuilder.baseUrl(baseUrl).build();
	}

	public <T> List<T> getAsList(String path, Class<T> responseElementClass) {
		return webClient.get().uri(path).retrieve().bodyToFlux(responseElementClass).collectList().block();
	}

	public <T> T get(String path, Class<T> responseElementClass) {
		return get(path, new LinkedMultiValueMap<>(), responseElementClass);
	}

	public <T> T get(String path, MultiValueMap<String, String> params, Class<T> responseElementClass) {
		return webClient.get()
				.uri(uriBuilder -> uriBuilder
						.path(path)
						.queryParams(params)
						.build())
				.retrieve()
				.bodyToMono(responseElementClass)
				.block();
	}
}
