package com.gmail.jordansilva.domain.services;

import com.gmail.jordansilva.domain.model.Currency;
import com.gmail.jordansilva.domain.model.Period;

public interface PriceIndexService {

	boolean isSupported(Currency currency);

	Double getRate(Currency currency);

	Double getHistoricalLow(Currency currency, Period period);

	Double getHistoricalHigh(Currency currency, Period period);
}
