package com.gmail.jordansilva.domain.services;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.max;
import static java.util.Collections.min;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.gmail.jordansilva.domain.model.Currency;
import com.gmail.jordansilva.domain.model.Period;
import com.gmail.jordansilva.domain.model.coinDesk.CurrentPriceResponse;
import com.gmail.jordansilva.domain.model.coinDesk.HistoricalPriceResponse;
import com.gmail.jordansilva.web.client.RestClient;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CoinDeskService implements PriceIndexService {

	private static final String CURRENT_PRICE_PATH = "currentprice/%s.json";
	private static final String SUPPORTED_CURRENCIES_PATH = "supported-currencies.json";
	private static final String HISTORICAL_PRICE_PATH = "historical/close.json";

	private final RestClient coinDeskClient;

	@Override
	public Double getRate(Currency currency) {
		CurrentPriceResponse response = getCurrentPrice(currency);
		return response.getBpi().getRate();
	}

	private CurrentPriceResponse getCurrentPrice(Currency currency) {
		String path = buildCurrentPricePath(currency);
		return coinDeskClient.get(path, CurrentPriceResponse.class);
	}

	private String buildCurrentPricePath(Currency currency) {
		return format(CURRENT_PRICE_PATH, currency.getCode().toUpperCase());
	}

	@Override
	public boolean isSupported(Currency currency) {
		List<Currency> supportedCurrencies = getSupportedCurrencies();
		return supportedCurrencies.contains(currency);
	}

	private List<Currency> getSupportedCurrencies() {
		return coinDeskClient.getAsList(SUPPORTED_CURRENCIES_PATH, Currency.class);
	}

	@Override
	public Double getHistoricalLow(Currency currency, Period period) {
		HistoricalPriceResponse response = getHistoricalPrice(currency, period);
		return findLowestValue(response);
	}

	private HistoricalPriceResponse getHistoricalPrice(Currency currency, Period period) {
		MultiValueMap<String, String> params = buildHistoricalPriceRequestParams(currency, period);
		return coinDeskClient.get(HISTORICAL_PRICE_PATH, params, HistoricalPriceResponse.class);
	}

	private MultiValueMap<String, String> buildHistoricalPriceRequestParams(Currency currency, Period period) {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.put("currency", asList(currency.getCode()));
		params.put("start", asList(period.getStart().toString()));
		params.put("end", asList(period.getEnd().toString()));
		return params;
	}

	private Double findLowestValue(HistoricalPriceResponse response) {
		return min(getPrices(response));
	}

	private Collection<Double> getPrices(HistoricalPriceResponse response) {
		return response.getBpi().values();
	}

	@Override
	public Double getHistoricalHigh(Currency currency, Period period) {
		HistoricalPriceResponse response = getHistoricalPrice(currency, period);
		return findHighestValue(response);
	}

	private Double findHighestValue(HistoricalPriceResponse response) {
		return max(getPrices(response));
	}
}
