package com.gmail.jordansilva.domain.model.coinDesk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurrentPriceResponse {
	private CurrentPriceBPI bpi;
}
