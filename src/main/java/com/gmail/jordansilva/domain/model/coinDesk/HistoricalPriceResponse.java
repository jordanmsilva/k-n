package com.gmail.jordansilva.domain.model.coinDesk;

import java.time.LocalDate;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoricalPriceResponse {
	private Map<LocalDate, Double> bpi;
}
