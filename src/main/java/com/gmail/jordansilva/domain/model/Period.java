package com.gmail.jordansilva.domain.model;

import java.time.LocalDate;

import lombok.Data;
import lombok.NonNull;

@Data
public class Period {

	@NonNull
	private final LocalDate start;

	@NonNull
	private final LocalDate end;
}
