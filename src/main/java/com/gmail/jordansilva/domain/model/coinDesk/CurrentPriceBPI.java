package com.gmail.jordansilva.domain.model.coinDesk;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonDeserialize(using = CurrentPriceBPIDeserializer.class)
public class CurrentPriceBPI {
	private Double rate;
}
