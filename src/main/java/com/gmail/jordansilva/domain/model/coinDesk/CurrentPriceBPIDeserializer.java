package com.gmail.jordansilva.domain.model.coinDesk;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class CurrentPriceBPIDeserializer extends StdDeserializer<CurrentPriceBPI> {

	private static final long serialVersionUID = 131247817677354245L;

	public CurrentPriceBPIDeserializer() {
		this(null);
	}

	protected CurrentPriceBPIDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public CurrentPriceBPI deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		final List<Double> rateList = new ArrayList<>();
		JsonNode node = jp.getCodec().readTree(jp);
		node.forEach(currencyNode -> {
			rateList.add(currencyNode.get("rate_float").doubleValue());
		});

		/*
		 * From studying the server response we know: 1. if asked for USD, only USD is
		 * in the response. 2. If asked for any other currency, USD and that currency is
		 * in the response 3. USD is always the first in the response
		 */
		Double lastRate = rateList.get(rateList.size() - 1);
		return new CurrentPriceBPI(lastRate);
	}
}