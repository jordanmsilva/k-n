package com.gmail.jordansilva.cli;

import com.gmail.jordansilva.domain.model.Currency;

public interface Messenger {

	public String buildEnterCurrencyMessage();

	public String buildCurrentRateMessage(Double rate);

	public String buildLast30DaysHighestRateMessage(Double rate);

	public String buildLast30DaysLowestRateMessage(Double rate);

	public String buildCurrencyNotSupportedMessage(Currency currency);
}
