package com.gmail.jordansilva.cli;

import static java.lang.String.format;

import org.springframework.stereotype.Component;

import com.gmail.jordansilva.domain.model.Currency;

@Component
public class EnglishMessenger implements Messenger {

	@Override
	public String buildEnterCurrencyMessage() {
		return "Enter the currency code (USD, EUR, BRL, etc): ";
	}

	@Override
	public String buildCurrentRateMessage(Double rate) {
		return format("Current rate: %s\n", formatRate(rate));
	}

	@Override
	public String buildLast30DaysHighestRateMessage(Double rate) {
		return format("Highest rate in the last 30 days: %s\n", formatRate(rate));
	}

	@Override
	public String buildLast30DaysLowestRateMessage(Double rate) {
		return format("Lowest rate in the last 30 days: %s\n", formatRate(rate));
	}

	@Override
	public String buildCurrencyNotSupportedMessage(Currency currency) {
		return format("Currency \"%s\" is not supported.\n", currency.getCode());
	}

	private String formatRate(Double rate) {
		return format("%,.4f", rate);
	}
}
