package com.gmail.jordansilva.cli;

import static java.time.LocalDate.now;

import java.time.LocalDate;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.gmail.jordansilva.Application;
import com.gmail.jordansilva.domain.model.Currency;
import com.gmail.jordansilva.domain.model.Period;

import lombok.RequiredArgsConstructor;

@Component
@Profile("!IntegrationTest")
@RequiredArgsConstructor
public class CommandLineInterface implements CommandLineRunner {

	private final Application app;
	private final Interactor interactor;
	private final Messenger messenger;

	@Override
	public void run(String... args) throws Exception {
		Currency currency = readCurrency();
		displayRateValues(currency);
	}

	private Currency readCurrency() {
		interactor.write(messenger.buildEnterCurrencyMessage());
		String currencyCode = interactor.read();
		return new Currency(currencyCode);
	}

	private void displayRateValues(Currency currency) {
		try {
			displayCurrentRate(currency);
			displayLast30DaysHighestRate(currency);
			displayLast30DaysLowestRate(currency);
		} catch (Application.UnsupportedCurrencyException ex) {
			displayCurrencyNotSupported(currency);
		}
	}

	private void displayCurrentRate(Currency currency) {
		Double rate = app.getRate(currency);
		interactor.write(messenger.buildCurrentRateMessage(rate));
	}

	private void displayLast30DaysHighestRate(Currency currency) {
		Double highest = app.getHistoricalHigh(currency, getLast30DaysPeriod());
		interactor.write(messenger.buildLast30DaysHighestRateMessage(highest));
	}

	private void displayLast30DaysLowestRate(Currency currency) {
		Double lowest = app.getHistoricalLow(currency, getLast30DaysPeriod());
		interactor.write(messenger.buildLast30DaysLowestRateMessage(lowest));
	}

	private void displayCurrencyNotSupported(Currency currency) {
		interactor.write(messenger.buildCurrencyNotSupportedMessage(currency));
	}

	private Period getLast30DaysPeriod() {
		LocalDate today = now();
		return new Period(today.minusDays(30), today);
	}
}
