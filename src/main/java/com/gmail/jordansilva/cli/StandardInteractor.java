package com.gmail.jordansilva.cli;

import java.util.Scanner;

import org.springframework.stereotype.Component;

@Component
public class StandardInteractor implements Interactor {

	private final Scanner scanner = new Scanner(System.in);

	@Override
	public String read() {
		return scanner.next();
	}

	@Override
	public void write(String content) {
		System.out.print(content);
	}
}
