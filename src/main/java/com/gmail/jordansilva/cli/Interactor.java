package com.gmail.jordansilva.cli;

public interface Interactor {

	public String read();

	public void write(String content);
}
