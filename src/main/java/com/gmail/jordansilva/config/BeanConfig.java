package com.gmail.jordansilva.config;

import static org.springframework.http.MediaType.ALL;

import org.springframework.boot.web.codec.CodecCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.jordansilva.web.client.RestClient;

@Configuration
public class BeanConfig {

	@Bean
	public CodecCustomizer textHtmlCodecCustomizer(ObjectMapper mapper) {
		return (clientCodecConfigurer) -> clientCodecConfigurer.customCodecs()
				.decoder(new Jackson2JsonDecoder(mapper, ALL));
	}

	@Bean
	public RestClient coinDeskClient(WebClient.Builder webClientBuilder, CoinDeskConfig config) {
		return new RestClient(webClientBuilder, config.getBaseUrl());
	}
}
