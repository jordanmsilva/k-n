package com.gmail.jordansilva.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties("coin-desk")
public class CoinDeskConfig {

	private String baseUrl;
}
