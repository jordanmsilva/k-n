package com.gmail.jordansilva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitcoinPriceIndexApplication {

	public static void main(String[] args) {
		SpringApplication.run(BitcoinPriceIndexApplication.class, args);
	}
}
