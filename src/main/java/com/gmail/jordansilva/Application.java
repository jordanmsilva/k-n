package com.gmail.jordansilva;

import org.springframework.stereotype.Component;

import com.gmail.jordansilva.domain.model.Currency;
import com.gmail.jordansilva.domain.model.Period;
import com.gmail.jordansilva.domain.services.PriceIndexService;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class Application {

	public static class UnsupportedCurrencyException extends RuntimeException {
		private static final long serialVersionUID = -8871116867047137091L;
	}

	private final PriceIndexService priceIndexService;

	/**
	 * Gets the real-time Bitcoin price in the specified currency.
	 * 
	 * @param currency the currency to convert into
	 * @return the Bitcoin price in the specified currency
	 * @throws UnsupportedCurrencyException if currency is not supported
	 */
	public Double getRate(Currency currency) {
		checkIfCurrencyIsSupported(currency);
		return priceIndexService.getRate(currency);
	}

	/**
	 * Gets the Bitcoin lowest price in the given period in the specified currency.
	 * 
	 * @param currency the currency to convert into
	 * @param period   the period to be considered in the search
	 * @return the Bitcoin price in the specified currency
	 * @throws UnsupportedCurrencyException if currency is not supported
	 */
	public Double getHistoricalLow(Currency currency, Period period) {
		checkIfCurrencyIsSupported(currency);
		return priceIndexService.getHistoricalLow(currency, period);
	}

	/**
	 * Gets the Bitcoin highest price in the given period in the specified currency.
	 * 
	 * @param currency the currency to convert into
	 * @param period   the period to be considered in the search
	 * @return the Bitcoin price in the specified currency
	 * @throws UnsupportedCurrencyException if currency is not supported
	 */
	public Double getHistoricalHigh(Currency currency, Period period) {
		checkIfCurrencyIsSupported(currency);
		return priceIndexService.getHistoricalHigh(currency, period);
	}

	private void checkIfCurrencyIsSupported(Currency currency) {
		if (!priceIndexService.isSupported(currency)) {
			throw new UnsupportedCurrencyException();
		}
	}
}
